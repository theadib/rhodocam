#!/usr/bin/env sh

# deployment for rhodocam

sudo cp rhodocam@pi.service /etc/systemd/system

sudo systemctl --system daemon-reload

sudo systemctl enable rhodocam@pi

sudo systemctl start rhodocam@pi

